package com.htrucci.petclicker;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;



public class MainActivity extends Activity {
	ImageButton clicker;
	SoundPool soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
	AudioManager audioManager;
	int dock;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dock = soundPool.load(this, R.raw.click, 1);
		clicker = (ImageButton)findViewById(R.id.clicker);
		
		clicker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				soundPool.play(dock, 1, 1, 0, 0, 1);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
